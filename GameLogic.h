namespace SL
{
	class WorldState;
	class GameObject;
	#ifndef GAM_L_
	#define GAM_L_
	class GameLogic {
		public:
			GameLogic();
			bool step();

	};
	#endif
}
