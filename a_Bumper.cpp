#include "a_Bumper.h"

using namespace SL;
using namespace SL::Behaviors;
using namespace BehaviorTree;

int leftbump = 8;
int rightbump = 7;
Metro m1 = Metro(1);

inline void Bumper::init(void* agent)
{

};
//Return True, se distância actual dada for maior/menor (bool)
//bool = false, distancia dada se for inferior à actual retorna SUCCESS
//bool = true, distancia dada se for superior à actual retorna SUCCESS
//else retorna RUNNING
Bumper::Bumper(bool _bump)
{
	this->bump = _bump;
	if(_bump){
		pinMode (leftbump, INPUT_PULLUP);
		//digitalWrite(leftbump, HIGH);
	}
	else{
		pinMode (rightbump, INPUT_PULLUP);
		//digitalWrite(rightbump, HIGH);
	}
	BLeft=0;
	BRight=0;
}

inline BEHAVIOR_STATUS Bumper::execute(void* agent)
{
	if (m1.check() == 1) {
		if(bump)
			return Bumper::LeftBumperStatus();
		else
			return Bumper::RightBumperStatus();
	}
	else 
		return BT_FAILURE; 
}

BEHAVIOR_STATUS Bumper::LeftBumperStatus(){
		BLeft = digitalRead(leftbump);
		if(BLeft == 1){
			return BT_SUCCESS;
		}
			return BT_FAILURE;
}

BEHAVIOR_STATUS Bumper::RightBumperStatus(){
	
	BRight = digitalRead(rightbump);
	if(BRight == 1){
		return BT_SUCCESS;
	}
		return BT_FAILURE;
	 	
	
}
