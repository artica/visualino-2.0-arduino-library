#ifndef TLED_H_
#define TLED_H_
#include "BehaviorTree.h"
//#include "Metro.h"
using namespace BehaviorTree;

namespace SL
{
	namespace Behaviors
	{
	const byte LED_PIN = 13;
	
	class TurnLed:public BehaviorTree::BehaviorTreeNode
	{

	private:
		bool a;
	public:
		BEHAVIOR_STATUS execute(void* agent);
		void init(void* agent);
		TurnLed(bool turnit);
		const BehaviorTree::BehaviorTreeList* getChildren() const
		{
			return NULL;
		}
		//void on();
		//void off();
		void HeartBit();
	};
	}

}
#endif
