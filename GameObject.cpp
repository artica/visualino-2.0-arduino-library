#include <iostream>
#include <string>
#include "GameObject.h"
#include "WorldState.h"
#include "BehaviorTree.h"

namespace SL {

	int GameObject::IDCount = 0;

	GameObject::GameObject()
	{
		ID = getID();
	};

	bool GameObject::doActions()
	{
		root->execute(this);
		return true;
	}

	GameObject::~GameObject()
	{
		std::cout << "destroyed" << std::endl;
	}

}
