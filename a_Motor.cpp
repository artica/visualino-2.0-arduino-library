#include "a_Motor.h"
using namespace SL;
using namespace SL::Behaviors;
using namespace BehaviorTree;

inline void Motor::init(void* agent)
{
	
};

Motor::Motor(int _motorA0, int _motorA1, int _speed)
{
	motorA0 = _motorA0;
	motorA1 = _motorA1;
	pinMode(motorA0, OUTPUT);
	pinMode(motorA1, OUTPUT);
	speed = _speed;
	digitalWrite(motorA0, LOW);
	digitalWrite(motorA1, LOW);
}

inline BEHAVIOR_STATUS Motor::execute(void* agent)
{
  	Serial.println(speed);

	Motor::MotorRun();
	return BT_SUCCESS;
}

inline void Motor::MotorRun() {
	if (speed > 0 ) {
		analogWrite(motorA0, speed);
		digitalWrite(motorA1, LOW);
	}
	if (speed < 0 ) {
		digitalWrite(motorA0, LOW);
		analogWrite(motorA1, map(speed, 0, -255, 0, 255));
	}
	if (speed == 0) {
		digitalWrite(motorA0, LOW);
		digitalWrite(motorA1, LOW);
	}  
}

