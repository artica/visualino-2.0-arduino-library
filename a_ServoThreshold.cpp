#include "a_ServoThreshold.h"

using namespace SL;
using namespace SL::Behaviors;
using namespace BehaviorTree;

//Servo sT;
//Metro servoDelayMetroT = Metro(250);

inline void ServoThreshold::init(void* agent) {
};

ServoThreshold::ServoThreshold(int _threshold, byte _lowhigh, int _pin)
{
	threshold = _threshold;
	lowhigh = _lowhigh;
	pin = _pin;
	//sT.attach(this->pin);
}

inline BEHAVIOR_STATUS ServoThreshold::execute(void* agent)
{
	int value = -1;
	//if (servoDelayMetroT.check() == 1)
	{
//todo: this doesnt work, needs reference to the object instance, which needs to be passed as initializing argument, or grabbed from root as a service of some sort 
//		value = SL::Behaviors::ServoIncrementor::sI.read();
		Serial.print(value);
		if (this->lowhigh == 0) {
			if (value < this->threshold) return BT_SUCCESS;
		}
		if (this->lowhigh == 1) {
			if (value > this->threshold) return BT_SUCCESS;
		}
	}
	
	return BT_FAILURE;
}
