#include "BehaviorTree.h"
using namespace BehaviorTree;
using namespace std;
void SequenceNode::init(void* agent)
{
	currentPosition = -1;
	for (BehaviorTreeListIter iter = children.begin(); iter!= children.end(); iter++)
		(*iter)->init(agent);
}

SequenceNode::SequenceNode()
{
	currentPosition = -1;
}


BEHAVIOR_STATUS SequenceNode::execute(void* agent)
{
	if (currentPosition == -1) //starting out
	{
		init(agent);
		currentPosition = 0;
	}

	if (children.size() == 0)
		return BT_SUCCESS;

	BehaviorTreeNode* currentTask = children.at(currentPosition);
	BEHAVIOR_STATUS result = currentTask->execute(agent);

	while(result == BT_SUCCESS)
	{
		if (currentPosition == children.size()-1) //finished last task
		{
			currentPosition = -1; //indicate we are not running anything
			return BT_SUCCESS;
		}
		else
		{
			currentPosition++;
			currentTask = children.at(currentPosition);
			result = currentTask->execute(agent);
		}
	}
	if (result == BT_FAILURE)
		currentPosition = -1;
	return result;
}
