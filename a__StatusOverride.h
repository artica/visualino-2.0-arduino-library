#pragma once
namespace BehaviorTree
{
///A node that repeats its child a specified number of times.
/** If the child returns BT_FAILURE, RepeatNode will also return BT_FAILURE. However, if the child returns BT_SUCCESS and it hasn't completed the specified number of repetitions, RepeatNode will restart it and continue returning BT_RUNNING. 
*/
class StatusOverride: public BehaviorTreeInternalNode
{
	public:
		BEHAVIOR_STATUS execute(void* agent);
		void init(void* agent);
		StatusOverride(int _succ_policy, int _fail_policy, int _runn_policy);
		//BehaviorTreeInternalNode* addChild(BehaviorTreeNode* newChild);
	private:
		int succ_policy;
		int fail_policy;
		int runn_policy;

	};
}
