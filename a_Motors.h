#ifndef MTRRS_H_
#define MTRRS_H_
#include "BehaviorTree.h"
#include "a_Metro.h"
using namespace BehaviorTree;

namespace SL
{
	namespace Behaviors
	{
	class Motors:public BehaviorTree::BehaviorTreeNode
	{

	private:
		byte maxSpeedLeft;
		byte maxSpeedRight;
		int speedLeft;
		int speedRight;
	public:

		BEHAVIOR_STATUS execute(void* agent);
		void init(void* agent);
		Motors(int _speedLeft, int _speedRight);
		void MotorDiffTurn(int _speedLeft, int _speedRight);
		const BehaviorTree::BehaviorTreeList* getChildren() const
		{
			return NULL;
		}
		
	};
	}

}
#endif
