
#include <Visualino.h>

void setup(){
	Serial.begin(9600);

	Farrusco* f = new Farrusco();
	BehaviorTree::ParallelNode* n1 = new BehaviorTree::ParallelNode(FAIL_ON_ALL,SUCCEED_ON_ONE);
	SL::Behaviors::Motor* n2 = new SL::Behaviors::Motor(3,5,200);
	SL::Behaviors::Motor* n3 = new SL::Behaviors::Motor(6,11,200);

	f->root = n1;
	n1->addChild(n2);
	n1->addChild(n3);

	ws = new WorldState();
	gl = new GameLogic();
	ws->insertObject(f);
}

void loop(){
	gl->step();
}
