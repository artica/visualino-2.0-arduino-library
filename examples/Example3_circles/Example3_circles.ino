
#include <Visualino.h>

void setup(){

	Serial.begin(9600);

	Farrusco* f = new Farrusco();

	BehaviorTree::ParallelNode* n1 = new BehaviorTree::ParallelNode(FAIL_ON_ALL,SUCCEED_ON_ONE);
	BehaviorTree::SequenceNode* n7 = new BehaviorTree::SequenceNode();
	BehaviorTree::ParallelNode* nd = new BehaviorTree::ParallelNode(FAIL_ON_ONE,SUCCEED_ON_ALL);
	SL::Behaviors::Motor* ne = new SL::Behaviors::Motor(3,5,255);
	SL::Behaviors::Motor* nf = new SL::Behaviors::Motor(6,11,255);
	SL::Behaviors::DelayTime* ng = new SL::Behaviors::DelayTime(2000);
	BehaviorTree::ParallelNode* nh = new BehaviorTree::ParallelNode(FAIL_ON_ONE,SUCCEED_ON_ALL);
	SL::Behaviors::Motor* ni = new SL::Behaviors::Motor(3,5,255);
	SL::Behaviors::Motor* nj = new SL::Behaviors::Motor(6,11,-255);
	SL::Behaviors::DelayTime* nk = new SL::Behaviors::DelayTime(200);

	f->root = n1;
	n1->addChild(n7);
	n7->addChild(nd);
	nd->addChild(ne);
	nd->addChild(nf);
	n7->addChild(ng);
	n7->addChild(nh);
	nh->addChild(ni);
	nh->addChild(nj);
	n7->addChild(nk);


	ws = new WorldState();
	gl = new GameLogic();
	ws->insertObject(f);
}

void loop(){
	gl->step();
}

