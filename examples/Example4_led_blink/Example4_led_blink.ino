
		#include <Visualino.h>
		
		void setup(){
  		
			Serial.begin(9600);
		
			Farrusco* f = new Farrusco();
		
			BehaviorTree::ParallelNode* n1 = new BehaviorTree::ParallelNode(FAIL_ON_ALL,SUCCEED_ON_ONE);
			BehaviorTree::SequenceNode* n7 = new BehaviorTree::SequenceNode();
			SL::Behaviors::LED* n8 = new SL::Behaviors::LED(13,255);
			SL::Behaviors::DelayTime* n9 = new SL::Behaviors::DelayTime(1000);
			SL::Behaviors::LED* na = new SL::Behaviors::LED(13,0);
			SL::Behaviors::DelayTime* nb = new SL::Behaviors::DelayTime(1000);

			f->root = n1;
			n1->addChild(n7);
			n7->addChild(n8);
			n7->addChild(n9);
			n7->addChild(na);
			n7->addChild(nb);

		
		    ws = new WorldState();
			gl = new GameLogic();
			ws->insertObject(f);
		}
		
		void loop(){
			gl->step();
		}
		