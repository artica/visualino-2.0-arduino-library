#ifndef SRI_H_
#define SRI_H_
#include "BehaviorTree.h"
#include "a_Metro.h"
#include "Servo.h"

using namespace BehaviorTree;

namespace SL
{
	namespace Behaviors
	{
	class ServoIncrementor:public BehaviorTree::BehaviorTreeNode
	{

	private:
		
		int max;
		int min;
		int inc;
		int pin;
		int servoPos;
		int servoInterval;
		
	public:

		Servo::Servo sI;
		
		BEHAVIOR_STATUS execute(void* agent);
		void init(void* agent);
		ServoIncrementor(int _inc, int _pin);
		const BehaviorTree::BehaviorTreeList* getChildren() const
		{
			return NULL;
		}
		
	};
	}

}
#endif
