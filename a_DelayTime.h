#ifndef TDL_H_
#define TDL_H_
#include "BehaviorTree.h"
#include "a_Metro.h"

using namespace BehaviorTree;

namespace SL
{
	namespace Behaviors
	{
	class DelayTime:public BehaviorTree::BehaviorTreeNode
	{
	private:
		int delay_time;
		bool b;
		long actual_time;
		long actual_time2;
		//Metro delayMetro;
	public:
		DelayTime(int dt);
		void init(void* agent);
		BEHAVIOR_STATUS execute(void* agent);
		const BehaviorTree::BehaviorTreeList* getChildren() const
		{
			return NULL;
		}
		
	};
	}

}
#endif
