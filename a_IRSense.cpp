#include "a_IRSense.h"

using namespace SL;
using namespace SL::Behaviors;
using namespace BehaviorTree;

Metro sensorIRMetro = Metro(1);
int IR_Pin = 0;
//int dist, max_min, IR_Pin, actual_dist;
inline void IRSense::init(void* agent)
{
};
//Return True, se distância actual dada for maior/menor (bool)
//bool = false, distancia dada se for inferior à actual retorna SUCCESS
//bool = true, distancia dada se for superior à actual retorna SUCCESS
//else retorna FAILURE
IRSense::IRSense(int _dist, bool _max_min)
{
	this->dist = _dist;
	this->max_min = _max_min;
	
}

inline BEHAVIOR_STATUS IRSense::execute(void* agent)
{
	if(sensorIRMetro.check()==1){
		return IRSense::IRDist();
	}
	else 
	return BT_FAILURE;
}

BEHAVIOR_STATUS IRSense::IRDist(){
		this->actual_dist = analogRead(IR_Pin);
		if(this->max_min){
			if(this->actual_dist < this->dist){
				return BT_SUCCESS;
			}
			else{
				return BT_FAILURE;
			}
		}
		else {
			if(this->actual_dist > this->dist){
				return BT_SUCCESS;
			}
			else{
				return BT_FAILURE;
			}
		}
}
