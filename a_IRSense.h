#ifndef IRS_H_
#define IRS_H_
#include "BehaviorTree.h"
#include "a_Metro.h"

using namespace BehaviorTree;

namespace SL
{
	namespace Behaviors
	{
	
	
	class IRSense:public BehaviorTree::BehaviorTreeNode
	{

	private:
		int dist;
		int max_min;
		int actual_dist;
	public:

		BEHAVIOR_STATUS execute(void* agent);
		void init(void* agent);
		IRSense(int _dist, bool _max_min);
		const BehaviorTree::BehaviorTreeList* getChildren() const
		{
			return NULL;
		}
		BEHAVIOR_STATUS IRDist();
	};
	}

}
#endif
