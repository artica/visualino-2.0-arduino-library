#include "a_ServoRange.h"

using namespace SL;
using namespace SL::Behaviors;
using namespace BehaviorTree;

Servo s1;
Metro servoDelayMetro = Metro(250);

inline void ServoRange::init(void* agent)
{
	max = 160;
	min = 20;
	inc = 1;
	pin = 9;
	servoPos = 90;
	servoInterval = 250;
	direction = INC_POS;
};

ServoRange::ServoRange(int _min, int _max, int _inc, int _pin)
{
	// make sure the first value is the lowest, and the second is the highest
	int m1 = _min;
	int m2 = _max;
	if (m2 < m1) {
		m1 = _max;
		m2 = _min;
	}
	
	// make sure min is not below our safe angle zone
	if (m1 < 20) {
		this->min = 20;
	} else {
		this->min = m1;
	}

	// make sure max is not above our safe angle zone
	if (m2 > 160) {
		this->max = 160;
	} else {
		this->max = m2;
	}
	
	// make sure our inc will never go beyond our safe angle zone
	int inct = _inc;
	if (inct > 20) inct = 20;
	// also make sure it's positive
	if (inct < 0) inct = 0;
	this->inc = inct;
	
	this->pin = _pin;
  	s1.attach(this->pin);
}

inline BEHAVIOR_STATUS ServoRange::execute(void* agent)
{
	// servo attach
	if (servoDelayMetro.check() == 1) {
		ServoRange::ServoRangeFunction();
	}
	return BT_RUNNING;
}

void ServoRange::ServoRangeFunction() 
{  
    //Serial.write(servoPos);
    s1.write(this->servoPos);

    if(this->servoPos >= this->max) this->direction = DEC_POS;
    if(this->servoPos <= this->min) this->direction = INC_POS;

    if(this->direction == INC_POS)
      this->servoPos += this->inc;
    else
      this->servoPos -= this->inc;

    servoDelayMetro.interval(this->servoInterval);
}
