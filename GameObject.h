#ifndef GAMEOBJECT_H_
#define GAMEOBJECT_H_
#include <list>
#include <string>
#include <set>
#include "BehaviorTree.h"

namespace SL
{
	class WorldState;
	class BehaviorTreeNode;
	class GameObject;

	/// Base class for any item in the game
	class GameObject{

		public:
			virtual bool doActions();

			GameObject();
			~GameObject();

			BehaviorTree::BehaviorTreeInternalNode* root;

			int ID;

			static int getID()
			{
				IDCount++;
				return IDCount;
			}

		protected:

		private:
			static int IDCount;

	};
}
#endif
