#pragma once
#include "BehaviorTree.h"

using namespace BehaviorTree;
using namespace std;

StatusOverride::StatusOverride(int _succ_policy, int _fail_policy, int _runn_policy)
{
	this->succ_policy = _succ_policy;
	this->fail_policy = _fail_policy;
	this->runn_policy = _runn_policy;
}

void StatusOverride::init(void* agent)
{
	for (BehaviorTreeListIter iter = children.begin(); iter!= children.end(); iter++)
		(*iter)->init(agent);
}

BEHAVIOR_STATUS StatusOverride::execute(void* agent)
{
	
	//init(agent);
	

	BehaviorTreeNode* currentTask = children.at(0);
	BEHAVIOR_STATUS result = currentTask->execute(agent);

	if(result == BT_SUCCESS){
		if(succ_policy == 1){
			return BT_FAILURE;
		}
		else if(succ_policy == 2){
			return BT_RUNNING;
		}
		else {
			return BT_SUCCESS;
		}
	}
	else if(result == BT_FAILURE){
		if(fail_policy == 1){
			return BT_SUCCESS;
		}
		else if(fail_policy == 2){
			return BT_RUNNING;
		}
		else{
			return BT_FAILURE;
		}
	}
	else if(result == BT_RUNNING){
		if(runn_policy==1){
			return BT_SUCCESS;
		}
		else if(runn_policy == 2){
			return BT_FAILURE;
		}
		else {
			return BT_RUNNING;
		}
	}
}


