#include "a_Sensor.h"

using namespace SL;
using namespace SL::Behaviors;
using namespace BehaviorTree;

Metro sensorMetro = Metro(1);

inline void Sensor::init(void* agent) {};

Sensor::Sensor(PIN_TYPE _mode, int _pin, int _threshold, bool _passtype)
{
	this->mode = _mode;
	this->pin = _pin;
	this->threshold = _threshold;
	this->passtype = _passtype;
}

inline BEHAVIOR_STATUS Sensor::execute(void* agent)
{
	if(sensorMetro.check() == 1){
		return Sensor::dist();
	}
	else 
	return BT_FAILURE;
}

BEHAVIOR_STATUS Sensor::dist(){
	int value = 0;
	switch(this->mode){
		case PIN_ANALOGUE:
			value = analogRead(this->pin); // 0 .. 1024
			
			if (this->passtype) {
				if(value > this->threshold){
					return BT_SUCCESS;
				}
				else{
					return BT_FAILURE;
				}
			} else {
				if(value < this->threshold){
					return BT_SUCCESS;
				}
				else{
					return BT_FAILURE;
				}
			}
		
		break;
		case PIN_DIGITAL:
			value = digitalRead(this->pin); // 0 .. 1
			if (this->passtype) {
				if( value == 0 ) return BT_FAILURE;
					else return BT_SUCCESS;
			} else {
				if( value != 0 ) return BT_FAILURE;
					else return BT_SUCCESS;
			}
		break;
	}
}
