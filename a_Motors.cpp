#include "a_Motors.h"
using namespace SL;
using namespace SL::Behaviors;
using namespace BehaviorTree;

// motor pins
int motorA0 = 6;
int motorA1 = 11;

int motorB0 = 3;
int motorB1 = 5;

// calibration variables, use it to perform a straight path
int min_speed = 100;
Metro m12 = Metro(40);
inline void Motors::init(void* agent)
{
	
};

Motors::Motors(int _speedLeft, int _speedRight)
{
	pinMode(motorA0, OUTPUT);
	pinMode(motorA1, OUTPUT);
	pinMode(motorB0, OUTPUT);
	pinMode(motorB1, OUTPUT);
	speedLeft = _speedLeft;
	speedRight = _speedRight;
	digitalWrite(motorB0, LOW);
	digitalWrite(motorB1, LOW);
	digitalWrite(motorA0, LOW);
	digitalWrite(motorA1, LOW);
}

inline BEHAVIOR_STATUS Motors::execute(void* agent)
{
  //if(m12.check() == 1) {
	  Motors::MotorDiffTurn(speedLeft,speedRight);
    return BT_SUCCESS;
  //}
  //else BT_FAILURE;
}



inline void Motors::MotorDiffTurn(int _speedLeft, int _speedRight) {

 if (speedLeft > 0) {  
  analogWrite(motorB0, speedLeft);
  digitalWrite(motorB1, LOW);
 }
 if (speedLeft < 0) {  
  digitalWrite(motorB0, LOW);
  analogWrite(motorB1, map(speedLeft, 0, -255, 0, 255));
 }
 if (speedLeft == 0) {  
  digitalWrite(motorB0, LOW);
  digitalWrite(motorB1, LOW); 
 }
 if (speedRight > 0 ) {
  analogWrite(motorA0, speedRight);
  digitalWrite(motorA1, LOW);
 }
 if (speedRight < 0 ) {
  digitalWrite(motorA0, LOW);
  analogWrite(motorA1, map(speedRight, 0, -255, 0, 255));
 }
 if (speedRight == 0) {
  digitalWrite(motorA0, LOW);
  digitalWrite(motorA1, LOW);
 }
  
}

