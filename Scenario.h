//#include "BehaviorTree.h"
#include "Farrusco.h"
#include "WorldState.h"

#ifndef SCENARIO1_H_
#define SCENARIO1_H_
using namespace SL;
using namespace SL::Behaviors;
namespace SL
{
	WorldState* loadScenario1()
	{
		WorldState* state = new WorldState();

		Farrusco* opponent = new Farrusco();
		//opponent->brain->addChild(new Printlnas())->addChild(new Printlnas2());
		state->insertObject(opponent);
		return state;
	}}
#endif