#include "WorldState.h"
#include "GameObject.h"

using namespace SL;
using namespace std;

WorldState::WorldState()
{
	description = "";

	activeObjectList = new GameObjectList();
	deleteList = new GameObjectList();
}

bool WorldState::insertObject(GameObject* gameObject)
{
	activeObjectList->push_front(gameObject);
	//currentList->push_front(gameObject);
	return true;
}
const GameObjectList* WorldState::getAllGameObjects() const
{
	return  activeObjectList; 
}

