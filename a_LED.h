#ifndef LED_H_
#define LED_H_
#include "BehaviorTree.h"
#include "a_Metro.h"
using namespace BehaviorTree;

namespace SL
{
	namespace Behaviors
	{
		class LED:public BehaviorTree::BehaviorTreeNode
		{
			private:
				bool a;
				int pin;
			public:
				BEHAVIOR_STATUS execute(void* agent);
				void init(void* agent);
				LED(int pin, bool turnit);
				const BehaviorTree::BehaviorTreeList* getChildren() const
				{
					return NULL;
				}
		};
	}
}
#endif
