#include "a_ServoIncrementor.h"

using namespace SL;
using namespace SL::Behaviors;
using namespace BehaviorTree;

Servo sI;
Metro servoDelayMetroI = Metro(250);

inline void ServoIncrementor::init(void* agent)
{
	this->max = 160;
	this->min = 20;
	this->inc = 1;
	this->pin = 9;
	this->servoPos = 90;
	servoInterval = 250;
};

ServoIncrementor::ServoIncrementor(int _inc, int _pin)
{
	//Serial.write("init: " + servoPos);
	int inct = _inc;

	this->inc = _inc;
	this->pin = _pin;
	this->sI = sI;
  	this->sI.attach(this->pin);
}

inline BEHAVIOR_STATUS ServoIncrementor::execute(void* agent)
{
	// servo attach
	if (servoDelayMetroI.check() == 1) {
		this->servoPos += this->inc;

		if(this->servoPos >= this->max) this->servoPos = this->max;
		if(this->servoPos <= this->min) this->servoPos = this->min;
	
		//Serial.print(this->servoPos);
		this->sI.write(this->servoPos);

		servoDelayMetroI.interval(this->servoInterval);
	}
	return BT_RUNNING;
}
