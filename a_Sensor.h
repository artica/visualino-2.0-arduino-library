#ifndef IRS_H_
#define IRS_H_
#include "BehaviorTree.h"
#include "a_Metro.h"

using namespace BehaviorTree;

namespace SL
{
	namespace Behaviors
	{
		class Sensor:public BehaviorTree::BehaviorTreeNode
		{
		private:
			PIN_TYPE mode;
			int pin;
			int threshold;
			bool passtype;
		public:
			BEHAVIOR_STATUS execute(void* agent);
			void init(void* agent);
			Sensor(PIN_TYPE _mode, int _pin, int _threshold, bool _passtype);
			const BehaviorTree::BehaviorTreeList* getChildren() const
			{
				return NULL;
			}
			BEHAVIOR_STATUS dist();
		};
	}

}
#endif
