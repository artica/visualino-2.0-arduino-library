#ifndef MTRR_H_
#define MTRR_H_
#include "BehaviorTree.h"
using namespace BehaviorTree;

namespace SL
{
	namespace Behaviors
	{
		class Motor:public BehaviorTree::BehaviorTreeNode
		{

		private:
			byte maxSpeed;
			int speed;
			int motorA0;
			int motorA1;
		public:
			BEHAVIOR_STATUS execute(void* agent);
			void init(void* agent);
			Motor(int _motorA0, int _motorA1, int _speed);
			void MotorRun();
			const BehaviorTree::BehaviorTreeList* getChildren() const
			{
				return NULL;
			}
		
		};
	}

}
#endif
