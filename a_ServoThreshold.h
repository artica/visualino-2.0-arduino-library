#ifndef SRT_H_
#define SRT_H_
#include "BehaviorTree.h"
#include "a_Metro.h"
#include "Servo.h"

using namespace BehaviorTree;

namespace SL
{
	namespace Behaviors
	{
	class ServoThreshold:public BehaviorTree::BehaviorTreeNode
	{

	private:
		
		int threshold;
		byte lowhigh;
		int pin;

	public:

		BEHAVIOR_STATUS execute(void* agent);
		void init(void* agent);
		ServoThreshold(int _threshold, byte _lowhigh, int _pin);
		const BehaviorTree::BehaviorTreeList* getChildren() const
		{
			return NULL;
		}
		void ServoThresholdFunction();
		
	};
	}

}
#endif
