#include "a_LED.h"

using namespace SL;
using namespace SL::Behaviors;
using namespace BehaviorTree;
using namespace std;

inline void LED::init(void* agent)
{
};

LED::LED(int pin, bool turnit)
{
	this->pin = pin;
	this->a = turnit;
}

inline BEHAVIOR_STATUS LED::execute(void* agent)
{
	if(a) {
		//pinMode(13, OUTPUT);
		digitalWrite(this->pin, HIGH);
		return BT_SUCCESS;
	} else {
	 	digitalWrite(this->pin,LOW);
	   	return BT_SUCCESS;
	}

}
