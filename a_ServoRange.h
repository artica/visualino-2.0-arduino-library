#ifndef SR_H_
#define SR_H_
#include "BehaviorTree.h"
#include "a_Metro.h"
#include "Servo.h"
#define INC_POS  0
#define DEC_POS  1

using namespace BehaviorTree;

namespace SL
{
	namespace Behaviors
	{
	class ServoRange:public BehaviorTree::BehaviorTreeNode
	{

	private:
		
		int max;
		int min;
		int inc;
		int pin;
		int servoPos;
		int servoInterval;
		byte direction;

	public:

		BEHAVIOR_STATUS execute(void* agent);
		void init(void* agent);
		ServoRange(int _min, int _max, int _inc, int _pin);
		const BehaviorTree::BehaviorTreeList* getChildren() const
		{
			return NULL;
		}
		void ServoRangeFunction();
	};
	}

}
#endif
