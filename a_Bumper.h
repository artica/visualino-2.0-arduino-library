#ifndef BMP_H_
#define BMP_H_
#include "BehaviorTree.h"
#include "a_Metro.h"

using namespace BehaviorTree;

namespace SL
{
	namespace Behaviors
	{
	
	
	class Bumper:public BehaviorTree::BehaviorTreeNode
	{

	private:
		int BLeft; 
		int BRight;
		bool bump;
	public:

		BEHAVIOR_STATUS execute(void* agent);
		void init(void* agent);
		Bumper(bool _bump);
		const BehaviorTree::BehaviorTreeList* getChildren() const
		{
			return NULL;
		}
		BEHAVIOR_STATUS LeftBumperStatus();
		BEHAVIOR_STATUS RightBumperStatus();
	};
	}

}
#endif
