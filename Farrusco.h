#ifndef FARR_H_
#define FARR_H_
//#include <list>
//#include <vector>
//#include <string>
//#include <set>
#include "GameObject.h"
#include "WorldState.h"
#include "BehaviorTree.h"
#include "a_TurnLed.h" // legacy code
#include "a_LED.h"
#include "a_ServoRange.h"
#include "a_ServoIncrementor.h"
#include "a_ServoThreshold.h"
#include "a_Motor.h"
#include "a_Motors.h" // legacy code
#include "a_Metro.h"
#include "a_DelayTime.h"
#include "a_Sensor.h"
#include "a_IRSense.h" // legacy code
#include "a_Bumper.h" // legacy code
using namespace BehaviorTree;

namespace SL
{
	class WorldState;

	class Farrusco: public GameObject {
		public:
			bool doActions();
			Farrusco();
			BehaviorTreeInternalNode* (*projectileBrain)();
	};

}
#endif
