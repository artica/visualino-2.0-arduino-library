#ifndef WORLDSTATE_H_
#define WORLDSTATE_H_

#include <list>
#include <queue>
#include "GameObject.h"
#include <set>

namespace SL
{
class GameObject;
/// Convenience typedef for a standard list of GameObjects
typedef std::list<GameObject*> GameObjectList;
/// Convenience typedef for a GameObjectList iterator
typedef GameObjectList::iterator GameObjectIter;
/// Convenience typedef for a const GameObjectList iterator
typedef GameObjectList::const_iterator ConstGameObjectIter;
/// Determines how finely the world is broken down into cells which hold GameObjects
/// A larger value means larger cells, and thus lower memory usage
#define coarseGraining 10

/// Data structure that maps a collection of objects into a 2d space
class WorldState
{
public:
/// Current time in milliseconds. The absolute value doesn't mean anything, only differences
unsigned long long time;
/// Stores the amount of real time between frames for e.g. physics updates
unsigned int timeElapsed;
/// Description of the level
std::string description;
/// Name of the level
std::string name;
/// ID of the level
std::string id;

/// Standard Constructor
WorldState();
/// Marks the object for deletion. The memory is not actually freed until a convenient time, and until then the object will continue to interact with things (right now)
bool deleteObject(GameObject* gameObject);
/// Get a list of all the active gameObjects in the world
const GameObjectList* getAllGameObjects() const;

bool insertObject(GameObject* gameObject);
private:
	GameObjectList* activeObjectList;
	GameObjectList* deleteList;
	void registerForDeletion(GameObject* obj);
	
};

}
#endif