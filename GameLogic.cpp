#include "GameLogic.h"
#include "GameObject.h"
#include "WorldState.h"

#include "globals.h"

using namespace SL;

GameLogic::GameLogic()
{}

bool GameLogic::step()
{

	const GameObjectList* objects = ws->getAllGameObjects();
	
	for( ConstGameObjectIter itr = objects->begin(); itr != objects->end(); ) {
		(*itr++)->doActions();
	}

	return true;
}
